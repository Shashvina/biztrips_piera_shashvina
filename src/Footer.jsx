// Footer.js
import React from "react";
import { getSeasonClass } from "./utilities"; // Assuming utilities.js contains the getSeasonClass function
import "./styling/pages.css";

export default function Footer({ season }) {
  const seasonClass = getSeasonClass(season);

  return (
    <footer className={`footer-${seasonClass}`}>
      <p>
        Connecting journeys with purpose-where business meets exploration and innovation.
      </p>
      <p>&copy; BBW 2024</p>
    </footer>
  );
}
