// Header.js
import React from "react";
import { useNavigate } from "react-router-dom";
import { getSeasonClass } from "./utilities"; // Assuming utilities.js contains the getSeasonClass function
import "./styling/pages.css";

export default function Header({ season }) {
  const navigate = useNavigate();
  const seasonClass = getSeasonClass(season);

  const handleSeasonChange = (event) => {
    const season = event.target.value;
    if (season) {
      switch (season) {
        case "0":
          navigate("/homepage");
          break;
        case "1":
          navigate("/winter");
          break;
        case "2":
          navigate("/fruehling");
          break;
        case "3":
          navigate("/sommer");
          break;
        case "4":
          navigate("/herbst");
          break;
        default:
          break;
      }
    }
  };

  return (
    <header className={`header-${seasonClass}`}>
      <nav>
        <ul>
          <li>
            <img width="150px" alt="Business Trips" src="/images/logo.png" />
            <label htmlFor="seasons"></label>{" "}
            <select id="seasons" onChange={handleSeasonChange}>
              <option value="0">Jahreszeiten</option>
              <option value="1">Winter</option>
              <option value="2">Frühling</option>
              <option value="3">Sommer</option>
              <option value="4">Herbst</option>
            </select>
          </li>
        </ul>
      </nav>
    </header>
  );
}
