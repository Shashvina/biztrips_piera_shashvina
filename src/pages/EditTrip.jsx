import React, { useState, useEffect } from "react";

const EditTrip = ({ trip, onSave }) => {
  const [formData, setFormData] = useState({
    id: trip.id,
    title: trip.title,
    description: trip.description,
    startTrip: trip.startTrip,
    endTrip: trip.endTrip,
  });

  useEffect(() => {
    setFormData({
      id: trip.id,
      title: trip.title,
      description: trip.description,
      startTrip: trip.startTrip,
      endTrip: trip.endTrip,
    });
  }, [trip]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSave(formData);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Title:</label>
        <input
          type="text"
          name="title"
          value={formData.title}
          onChange={handleChange}
        />
      </div>
      <div>
        <label>Description:</label>
        <textarea
          name="description"
          value={formData.description}
          onChange={handleChange}
        />
      </div>
      <div>
        <label>Start Date:</label>
        <input
          type="date"
          name="startTrip"
          value={formData.startTrip}
          onChange={handleChange}
        />
      </div>
      <div>
        <label>End Date:</label>
        <input
          type="date"
          name="endTrip"
          value={formData.endTrip}
          onChange={handleChange}
        />
      </div>
      <button type="submit">Save</button>
    </form>
  );
};

export default EditTrip;
