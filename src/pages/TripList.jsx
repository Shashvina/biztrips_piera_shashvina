import React, { useState } from "react";
import { Link } from "react-router-dom";
import { FaEdit, FaTrash } from "react-icons/fa";
import "./../styling/TripList.css";
import { IMAGE_PATHS } from "./constants";
import Header from "./../Header";
import Footer from "./../Footer";
import { deleteTrip, updateTrip } from "./../services/tripService";

const TripList = ({ tripList = [], deleteFromTripList, editTripInTripList }) => {
  const [editingTrip, setEditingTrip] = useState(null);
  const [editedTripDetails, setEditedTripDetails] = useState({
    id: "",
    title: "",
    description: "",
    startTrip: "",
    endTrip: "",
    persons: "",
    allergies: "",
    injuries: "",
    specialRequests: "",
  });

  const handleEditChange = (event) => {
    const { name, value } = event.target;
    setEditedTripDetails({
      ...editedTripDetails,
      [name]: value,
    });
  };

  const handleEditSubmit = async (event) => {
    event.preventDefault();
    try {
      await updateTrip(editedTripDetails.id, editedTripDetails);
      editTripInTripList(editedTripDetails);
      setEditingTrip(null);
    } catch (error) {
      console.error("Error updating trip", error);
    }
  };

  const startEditing = (trip) => {
    setEditingTrip(trip.id);
    setEditedTripDetails(trip);
  };

  const handleDelete = async (tripId) => {
    try {
      await deleteTrip(tripId);
      deleteFromTripList(tripId);
    } catch (error) {
      console.error("Error deleting trip", error);
    }
  };

  return (
    <div className="trip-list-container">
      <Header />
      <nav className="navigation">
        <Link to="/" className="nav-link">Formular</Link>
        <span>|</span>
        <Link to="/homepage" className="nav-link">Homepage</Link>
      </nav>
      <main className="content">
        <h1>Trip List</h1>
        {tripList.length === 0 ? (
          <p>No trips added to the list.</p>
        ) : (
          <div className="trip-list">
            {tripList.map((trip) => (
              <div key={trip.id} className="trip-item">
                <img
                  src={trip.imagePath || `${IMAGE_PATHS.tripList}${trip.id}.png`}
                  alt={trip.title}
                  className="trip-image"
                />
                {editingTrip === trip.id ? (
                  <form onSubmit={handleEditSubmit} className="edit-form">
                    <input
                      type="text"
                      name="title"
                      value={editedTripDetails.title}
                      onChange={handleEditChange}
                      readOnly
                    />
                    <input
                      type="text"
                      name="description"
                      value={editedTripDetails.description}
                      onChange={handleEditChange}
                      readOnly
                    />
                    <input
                      type="date"
                      name="startTrip"
                      value={editedTripDetails.startTrip}
                      onChange={handleEditChange}
                    />
                    <input
                      type="date"
                      name="endTrip"
                      value={editedTripDetails.endTrip}
                      onChange={handleEditChange}
                    />
                    <input
                      type="number"
                      name="persons"
                      value={editedTripDetails.persons}
                      onChange={handleEditChange}
                      placeholder="Number of persons"
                    />
                    <input
                      type="text"
                      name="allergies"
                      placeholder="Allergies"
                      value={editedTripDetails.allergies}
                      onChange={handleEditChange}
                    />
                    <input
                      type="text"
                      name="injuries"
                      placeholder="Injuries"
                      value={editedTripDetails.injuries}
                      onChange={handleEditChange}
                    />
                    <input
                      type="text"
                      name="specialRequests"
                      placeholder="Special Requests"
                      value={editedTripDetails.specialRequests}
                      onChange={handleEditChange}
                    />
                    <div className="button-group">
                      <button type="submit" className="btn-save">Save</button>
                      <button type="button" className="btn-cancel" onClick={() => setEditingTrip(null)}>
                        Cancel
                      </button>
                    </div>
                  </form>
                ) : (
                  <>
                    <div className="trip-info">
                      <h2>{trip.title}</h2>
                      <p>{trip.description}</p>
                      <p>From: {trip.startTrip}</p>
                      <p>To: {trip.endTrip}</p>
                      <p>Persons: {trip.persons}</p>
                      <p>Allergies: {trip.allergies}</p>
                      <p>Injuries: {trip.injuries}</p>
                      <p>Special Requests: {trip.specialRequests}</p>
                    </div>
                    <div className="button-group">
                      <button className="btn-delete" onClick={() => handleDelete(trip.id)}>
                        <FaTrash /> Delete
                      </button>
                      <button className="btn-edit" onClick={() => startEditing(trip)}>
                        <FaEdit /> Edit
                      </button>
                    </div>
                  </>
                )}
              </div>
            ))}
          </div>
        )}
      </main>
      <Footer />
    </div>
  );
};

export default TripList;
