import axios from 'axios';

const API_URL = 'http://localhost:3000/trips'; // URL zu deiner db.json Datei, wenn du json-server benutzt

export const getTrips = () => {
  return axios.get(API_URL);
};

export const addTrip = (trip) => {
  return axios.post(API_URL, trip);
};

export const updateTrip = (id, updatedTrip) => {
  return axios.put(`${API_URL}/${id}`, updatedTrip);
};

export const deleteTrip = (id) => {
  return axios.delete(`${API_URL}/${id}`);
};
