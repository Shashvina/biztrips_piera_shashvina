// utilities.js (neue Datei)
export function getSeasonClass(season) {
    switch (season) {
      case 'spring':
        return 'spring';
      case 'summer':
        return 'summer';
      case 'autumn':
        return 'autumn';
      case 'winter':
        return 'winter';
      case 'normal':
        return 'normal';
      case 'home':
        return 'home';    
      default:
        return '';
    }
  }
  